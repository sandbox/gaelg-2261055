<?php
/**
 * @file
 * Variable hooks for gcc_variable.
 */


/**
 * Implements hook_variable_info_alter().
 *
 * Only variables which have the "gcc_dependent" property set to TRUE, can be
 * used as group specific variables. Here we set this property for a set of
 * default system variables.
 */
function gcc_variable_variable_info_alter(&$variables, $options) {
  $list = array(
    'site_name',
    'site_slogan',
    'site_mail',
    'theme_settings',
    'theme_[theme]_settings',
    'admin_theme',
    'maintenance_mode',
    'maintenance_mode_message',
  );
  foreach ($list as $name) {
    if (isset($variables[$name])) {
      $variables[$name]['gcc_dependent'] = TRUE;
    }
  }
}