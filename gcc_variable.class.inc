<?php
/**
 * @file
 * Variable Realm controller class definition.
 */

/**
 * Realm controller for GCC realms.
 */
class GCCVariableRealmController extends VariableRealmDefaultController {
  /**
   * Implementation of VariableRealmControllerInterface::getAvailableVariables().
   */
  public function getAvailableVariables() {
    $gcc_dependent = array();
    foreach (variable_get_info() as $name => $variable) {
      if (!empty($variable['gcc_dependent'])) {
        $gcc_dependent[] = $name;
      }
    }
    return $gcc_dependent;
  }

  /**
   * Implementation of VariableRealmControllerInterface::getDefaultKey().
   */
  public function getDefaultKey() {
    return 'no_gcc_context';
  }

  /**
   * Implementation of VariableRealmControllerInterface::getRequestKey().
   */
  public function getRequestKey() {
    if ($context = gcc_context_get_context()) {
      return _gcc_variable_realm_key($context['entity_type'], $context['entity_id']);
    }
    return 'no_gcc_context';
  }

  /**
   * Implementation of VariableRealmControllerInterface::getAllKeys().
   */
  public function getAllKeys() {
    $list = gcc_group_get_list();
    $keys = array('no_gcc_context' => t('No group context available (default)'));
    foreach ($list as $group) {
      $group = (array)$group;
      if ($entity = reset(entity_load($group['entity_type'], array($group['entity_id'])))) {
        $keys[_gcc_variable_realm_key($group['entity_type'], $group['entity_id'])] = entity_label($group['entity_type'], $entity);
      }
    }
    return $keys;
  }

}
