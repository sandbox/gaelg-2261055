<?php

/**
 * Variables settings form
 */
function gcc_variable_admin_variables($entity_type, $entity) {
  $realm_key = _gcc_variable_realm_key($entity_type, entity_id($entity_type, $entity));
  module_load_include('form.inc', 'variable_realm');
  return drupal_get_form('variable_realm_edit_variables_form', 'gcc', $realm_key);
}
